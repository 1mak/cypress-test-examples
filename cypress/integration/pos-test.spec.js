
const baseUrl = "http://www.vinum.torsten.lan:8080";
const cartPrice = '110.00';

describe("pos test", () => {
  
  function setup() {
    cy.clearCookies();
    
    cy.server();
    cy.route("GET", "/vuejs/ecommerce/config").as("config-ready");
    
    cy.visit(baseUrl + "/pos");
    cy.get("input[type=email]").type("developer");
    cy.get("input[type=password]").type("develpwd,.,.,.654321");
    cy.get("a.submit").click();
  
    cy.wait("@config-ready");
    cy.contains("Please set up your Terminal").should("exist");
    cy.get("#pos_channel").select("External Events");
    cy.get("#pos_terminal").select("Event Pos");
    cy.get(".btn-confirm").click();
  }
  
  function checkCartClear() {
    cy.get('.checkout-items li').should("not.exist")
  }
  
  function addProductToCart() {
    cy.get(".product-container:first-child").click();
  }
  
  function checkCartPrices(price) {
    
    // Check Cart Item Price
    cy.get('.checkout-items li .price .amount').then(amount => {
      expect(amount.text().trim()).to.eql(price);
    })
    
    // Check GST Tax Price
    cy.get('.cost-summary .misc-charges .misc-charge-aust_gst .price strong').should("exist");
  
  
    // Check Cart Total Price
    cy.get('.cost-summary .price-total .price').contains(price).should("exist");
    
  }
  
  // it("Add item to cart twice", () => {
  //   setup();
  //
  //   checkCartClear();
  //   addProductToCart();
  //   addProductToCart();
  //
  //   // Promise.all([
  //   //   cy.get(".product-container:first-child .product:first-child .amount"),
  //   //   cy.get(".product-container:first-child .product:first-child .amount")
  //   //   ])
  //   //   .then(amounts => {
  //   //     //expect(amount.text().trim()).to.eql("55.00");
  //   //     console.log("hello")
  //   //     console.log(amounts);
  //   //   });
  //   cy.get('.checkout-items li .quantity .number').then(number => {
  //     expect(number.length).to.eql(1);
  //     expect(number.text().trim()).to.eql("2x");
  //   });
  //
  //   checkCartPrices(cartPrice);
  //
  //   cy.get('.checkout-button').click();
  //   cy.get('.payment-method.cash').click();
  //   cy.get('.checkout-step-title').contains('Cash Payment').should('exist');
  //   cy.get('.cash-total').contains(cartPrice).should('exist');
  //   cy.get('.cash-payment-option:first-child').click();
  //   cy.get('.cash-payment-container .btn-confirm').click();
  //   cy.get('.checkout-steps-container').contains('Review Payment').should('exist');
  //   cy.get('.checkout-steps-container .review-payment-container .btn-confirm.continue-button').click();
  //
  //   // cy.get("input").then(inputEl => {
  //   //   expect(inputEl.value()).to.eql("Hello");
  //   // });
  //
  // });
  //
  // it("Select a customer", () => {
  //   setup();
  //
  //   cy.get('.customer-actions .options .btn-primary:first-child').click();
  //   cy.get('.find-customer .found-customers ul li:first-child .options .btn-primary').click();
  //   cy.get('.customer-actions h3 .title').contains('Guest').should('not.exist');
  //
  //   // Check that the cart has been cleared
  //   cy.get('.order-summary .no-items').should('exist');
  // });
  //
  // it("Select a guest", () => {
  //
  //   cy.get('.customer-actions .options .btn-primary:first-child').click();
  //   cy.get('.find-customer .clear-customer-button').click();
  //   cy.get('.customer-actions h3').contains('Guest Checkout').should('exist');
  //
  //   // Check that the cart has been cleared
  //   cy.get('.order-summary .no-items').should('exist');
  // });
  
  it("Add item to cart and put order on hold", () => {
    setup();
    addProductToCart();
    cy.get('.checkout-container .pause-button').click();
    cy.get('.modal-body .park-order').should('exist');
    cy.get('.modal-body .park-order input[type=text]').clear().type('Test Order');
    cy.get('.modal-body .park-order .btn-primary').click();
    cy.get('.checkout-container .pause-button').click();
    cy.get('.modal-body .park-order .parked-orders-list .park-order-item h4').contains('Test Order').should('exist');
    cy.get('.modal-body .park-order .parked-orders-list .park-order-item .resume-options .btn-primary').click();
  });
  
});
